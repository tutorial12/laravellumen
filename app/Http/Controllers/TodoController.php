<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class TodoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function index() {
        $data = Todo::all();
        return response($data);
    }

    public function show($id) {
        $data = Todo::where('id',$id)->get();
        return response($data);
    }

    public function store (Request $request) {
        $data = new Todo();
        $data->activity = $request->input('activity');
        $data->description = $request->input('description');
        $data->save();

        return response('Post Data Success');
    }
}
